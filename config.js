// boygiandi - 2018
const  Config = {
	port : 3333,
	app_name: "gostream",
	base_domain: "https://gostream.vn",
	sub_path: "/api",
	secret_string: "^*B(H()",
	origin: ["http://localhost:8080", "https://gostream.vn", "https://nhanhre.gostream.vn", "*"],
	error : {
		"controller_not_found"	: [101, "Controller not found"],
		"action_not_found"		: [102, "Action not found"],
		"invalid_checksum"		: [103, "Invalid checksum"]
	},
	debug: true,
	white_list: [
		"/api/transaction/paypal",
		"/api/transaction/vtc/"
	]
}

module.exports = Config;