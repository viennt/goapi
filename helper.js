const fetch = require('node-fetch');
const md5 = require('md5');
const config = require('./config.js');
const uuid = require('uuid/v4');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('GoStudioGoStream');

module.exports = {
	now: function() {
		const date = new Date();
		return parseInt(date.getTime()/1000);
	},
	api: async function(path, data=null, showError=false) {
		function generate_signature(args) {
			let tmp_string = args.time+config.secret_string;
			if ( args.data ) tmp_string += args.data;
			return `time=${args.time}&sig=`+md5(tmp_string);
		}
		const date = new Date();
		
		if ( data && typeof data!=="string" ) data = JSON.stringify(data);
		let query = generate_signature({time: this.now(), data})
		const url = config.streamer_gateway+"/"+path+(path.indexOf('?')>0?'&':'?')+query;
		let request_config = {};
		if ( data ) {
			request_config['method'] = 'POST';
			request_config['body'] = data;
		}
		
		return fetch(url, request_config).then(response => response.json())
	},
	today: function() {
		let today = new Date();
		let dd = today.getDate();
		let mm = today.getMonth() + 1; //January is 0!

		let yyyy = today.getFullYear();
		if (dd < 10) {
			dd = '0' + dd;
		} 
		if (mm < 10) {
			mm = '0' + mm;
		}
		return dd + '/' + mm + '/' + yyyy;
	},
	padDigits: function(number, digits) {
			return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
	},
	validateEmail: function(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(String(email).toLowerCase());
	},
	loginFB: function(email, password) {
		const sim = this.randBetween(2e4, 4e4);
		let deviceID = uuid();
		let adID = uuid();
		let formData = {
			adid: adID,
			format: 'json',
			device_id: deviceID,
			email: email,
			password: password,
			cpl: 'true',
			family_device_id: deviceID,
			credentials_type: 'device_based_login_password',
			generate_session_cookies: '1',
			error_detail_type: 'button_with_disabled',
			source: 'device_based_login',
			machine_id: this.randString(24),
			meta_inf_fbmeta: '',
			advertiser_id: adID,
			currently_logged_in_userid: '0',
			locale: 'en_US',
			client_country_code: 'US',
			method: 'auth.login',
			fb_api_req_friendly_name: 'authenticate',
			fb_api_caller_class: 'com.facebook.account.login.protocol.Fb4aAuthHandler',
			api_key: '882a8490361da98702bf97a021ddc14d'
		};
		formData.sig = this.getSig(this.sortObj(formData));
		let conf = {
			method: 'post',
			body: JSON.stringify(formData),
			headers: {
				'x-fb-connection-bandwidth': this.randBetween(2e7, 3e7),
				'x-fb-sim-hni': sim,
				'x-fb-net-hni': sim,
				'x-fb-connection-quality': 'EXCELLENT',
				'x-fb-connection-type': 'cell.CTRadioAccessTechnologyHSDPA',
				'user-agent':
					'Dalvik/1.6.0 (Linux; U; Android 4.4.2; NX55 Build/KOT5506) [FBAN/FB4A;FBAV/106.0.0.26.68;FBBV/45904160;FBDM/{density=3.0,width=1080,height=1920};FBLC/it_IT;FBRV/45904160;FBCR/PosteMobile;FBMF/asus;FBBD/asus;FBPN/com.facebook.katana;FBDV/ASUS_Z00AD;FBSV/5.0;FBOP/1;FBCA/x86:armeabi-v7a;]',
				'content-type': 'application/x-www-form-urlencoded',
				'x-fb-http-engine': 'Liger',
				'content-type' : 'application/json'
			}
		};
		return fetch("https://b-api.facebook.com/method/auth.login", conf).then(response => response.json())
	},
	sortObj: function(obj) {
		let keys = Object.keys(obj).sort(),
			sortedObj = {};
		for (let i in keys) {
			sortedObj[keys[i]] = obj[keys[i]];
		}
		return sortedObj;
	},

	randBetween: function(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	},

	randString: function(limit) {
		limit = limit || 10;
		let text = 'abcdefghijklmnopqrstuvwxyz';
		text = text.charAt(Math.floor(Math.random() * text.length));
		const possible = 'abcdefghijklmnopqrstuvwxyz0123456789';
		for (let i = 0; i < limit - 1; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		return text;
	},
	getSig: function(formData) {
		let sig = '';
		Object.keys(formData).forEach(function(key) {
			sig += `${key}=${formData[key]}`;
		});
		sig = md5(sig + '62f8ce9f74b12f84c123cc23437a4a32');
		return sig;
	},
	encode: function(str) {
		return cryptr.encrypt(str);
	},
	decode: function(str) {
		return cryptr.decrypt(str);
	}
}