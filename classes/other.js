const fetch = require('node-fetch');
const helper = require('../helper');

class Other {
	constructor(parent) {
		this.parent = parent;
	}

	getName() {
		return "Other"
	}

	GET(db, args) {
		console.log("get_hello");
		return Promise.resolve(args);
	}

	POST(db, args) {
		console.log("post hello");
		return Promise.resolve(args);
	}

	async GET_server(db, args) {
		if ( !args['userInfo'] || !args['userInfo']['uid'] ) {
			return false;
		}
		return {};
	}
};

module.exports = Other;