#!/bin/bash
var functions = require('firebase-functions');
var express = require('express');
var config = require('./config.js');
var controller = new (require(`./controller.js`))(__dirname+"/classes", config);
var admin = require('firebase-admin');
var db = null;
var args = process.argv.slice(2);
admin.initializeApp(functions.config().firebase);
var db = admin.firestore();
db.settings({timestampsInSnapshots: true});
controller.fb_admin = admin;

var app = express();

function serve(controll, action, args) {
	if ( !controller[controll] )
		return Promise.reject(controller.returnError("controller_not_found"));
	if ( !controller[controll][action] )
		return Promise.reject(controller.returnError("action_not_found"));
	
	return controller[controll][action](db, args);
}
function valid_args(req) {
	let date = new Date();
	req.query['now'] = parseInt(date.getTime()/1000);
	
	const valid_request = controller.validRequest(req);
	if ( valid_request.error )
		return Promise.reject(valid_request);

	try {
		if ( req.body && typeof req.body==="string" ) 
			req.body = JSON.parse(req.body);
	} catch(e) {
		req.body = {raw: req.body};
	}
	let args = Object.assign(req.query, req.body);
	args['host'] = (req.headers['x-forwarded-host'] || req.headers.host).split(":").shift();
	args['clientip'] = req.headers['fastly-client-ip'] || 
							req.headers['x-forwarded-for'] || 
							(req.connection.remoteAddress ? req.connection.remoteAddress.replace("::ffff:", "") : '');
	args['clientip'] = args['clientip'].split(",").shift().trim();

	return Promise.resolve(args);
}

app.use('/static', express.static('static'));

app.all('*', function (req, res) { // config['sub_path']+'/:controller/:action*?'
	if ( req.originalUrl.indexOf(config['sub_path'])==-1 ) {
		res.statusCode = 404;
		res.send("404 not found");
		return;
	}
	let part = req.originalUrl.split(config['sub_path']+'/').pop().split("?").shift().split("/");
	req.params = {
		controller: part[0],
		action: part[1]
	}
	const origin = req.get('Origin');
	if ( config['origin'].indexOf('*')>-1 )
		res.setHeader("Access-Control-Allow-Origin", "*");
	else if ( config['origin'].indexOf(origin)>-1 )
		res.setHeader("Access-Control-Allow-Origin", origin);
	if (req.method === 'OPTIONS') {
		res.setHeader("Access-Control-Allow-Credentials", "true");
		res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
		res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, X-Auth-Token");
		res.send(200);
		return;
	}

	valid_args(req).then(async (args) => {
		const action = req.method + (req.params['action']?("_"+req.params['action']):"");
		const authorization = req.get('Authorization');
		if ( authorization ) {
			const tokenId = authorization.split('Bearer ')[1];
			args.userInfo = await admin.auth().verifyIdToken(tokenId);
		}

		return serve(req.params['controller'], action, args).then(response => {
			if ( response.error )
				throw response;
			else
				res.send(response);
		});
	}).catch((error) => {
		res.statusCode = 500;
		res.send(controller.returnError(error));
	});
});

// app.get('/*', function (req, res) {
// 	res.statusCode = 404;
// 	res.send("404 not found");
// });
// app.post('/*', function (req, res) {
// 	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
// 	console.log(config['sub_path'], fullUrl);
// 	res.statusCode = 404;
// 	res.send("404 not found");
// });


exports.app = functions.https.onRequest(app);

for ( let name in controller ) {
	if ( controller[name].listen && controller[name].listen.length ) {
		let listeners = {};
		for ( let i=0; i<controller[name].listen.length; i++ )
			listeners[controller[name].listen[i]] = controller[name][controller[name].listen[i]](functions, db);
		Object.assign(exports, listeners);
	}
}

if ( args[0]==='local' ) {
	app.listen(config.port);
	console.log(`Application running on port ${config.port}!`);
}
